This Repo is to test Ansible, Gitlab and Jenkins Integration
------------------------------------------------------------
Sample for Jenkins Project:

-> Ansible, Gitlab and Jenkins ==> to create a project

-> Fork yogesh's gitlab repo "https://gitlab.com/yogeshraheja/jenkinstest"
-> Create a new project using your jenkins console
-> Give it a name "ansibledemo"
-> Under SCM --> put your gitlab repo link here
-> Change the master branch to main
-> Scroll down and under the BUILD section --> ADD BUILD --> Invoke Ansible Playbook
-> Under the playbook path --> websetup.yml
-> Select Inventory --> FILE OR HOST LIST ---> and put here the inventory name "myinventory"
-> Click Save Button
-> Build your project 
-> Make sure you are having port 80 opened in your firewall rule
-> Take thge public ip and go to browser and you will see the webpage configured by Ansible Playbook
